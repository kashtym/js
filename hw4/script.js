//Напиши функцію map(fn, array), яка приймає на вхід функцію та масив, та обробляє кожен елемент масиву цією функцією, повертаючи новий масив

function map(fn, array) {
   let resultArr = [];
   for (let i = 0; i < array.length; i++) {
      resultArr.push(fn(array[i]));
   }
   return resultArr;
}

let arr = [1, 2, 3, 4, 5];

function double(x) {
   return x * 2;
}

console.log(arr);
console.log(map(double, arr));

//Використовуючи CallBack function, створіть калькулятор, який буде від користувача приймати 2 числа і знак арифметичної операції.При введенні не числа або при розподілі на 0 виводити помилку



function add(x, y) {
   return x + y;
}

function sub(x, y) {
   return x - y;
}

function mult(x, y) {
   return x * y;
}

function div(x, y) {
   return x / y;
}

function equal(x, y, CallBack) {
   return CallBack(x, y);
}

function resultFunc(x, y, oper) {
   let res = Error;
   if (!isNaN(x) && !isNaN(y)) {
      switch (oper) {
         case "+":
         res = equal(x, y, add);
         break;
         case "-":
            res = equal(x, y, sub);
            break;
         case "*":
            res = equal(x, y, mult);
            break;
         case "/":
            if(y != 0) {
               res = equal(x, y, div);
            }
      }
   }
   return res;
}

let num1 = parseFloat(prompt("Введіть число №1"));
let num2 = parseFloat(prompt("Введіть число №2"));
let operation = prompt("Введіть математичну операцію з перелічених +,-,*,/", "+");
let result = resultFunc(num1, num2, operation);

document.write((result != Error)?`${num1} ${operation} ${num2} = ${result}`:"ERROR");