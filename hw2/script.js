//Використовуючи пробіли &nbsp; та зірочки * намалюйте використовуючи цикли

//Висота фігур
let height = parseInt(prompt("Вкажіть висоту фігури в умовних одиницях", "9"));
document.write(`Висота всих фігур = ${height} <br> <hr>`);

//Трикутник прямокутний 1
document.write("Трикутник прямокутний 1 <br> <br>");
for (let i = 0; i < height; i++) {
   for (let j = height - i; j <= height; j++) {
      document.write('*');
   }
   document.write("<br>");
}
document.write("<hr>");

//Трикутник прямокутний 2
document.write("Трикутник прямокутний 2 <br> <br>");
for (let i = 0; i < height; i++) {
   for (let j = i; j < height - 1; j++) {
      document.write('&nbsp');
   }
   for (let j = height - i; j <= height; j++) {
      document.write('*');
   }
   document.write("<br>");
}
document.write("<hr>");

//Трикутник рівнобедрений
document.write("Трикутник рівнобедрений 2 <br> <br>");
for (let i = 0; i < height; i++) {
   for (let j = i; j < height - 1; j++) {
      document.write('&nbsp');
   }
   for (let j = height - i * 2; j <= height; j++) {
      document.write('*');
   }
   for (let j = i; j < height - 1; j++) {
      document.write('&nbsp');
   }
   document.write("<br>");
}

document.write("<hr>");

//Ромб
document.write("Ромб <br> <br>");
for (let i = 0; i < height; i++) {
   if (i < height / 2) {
      for (let j = i; j < height - 1; j++) {
         document.write('&nbsp');
      }
      for (let j = height - i * 2; j <= height; j++) {
         document.write('*');
      }
      for (let j = i; j < height - 1; j++) {
         document.write('&nbsp');
      }
      document.write("<br>");
   } else {
      for (let j = i; j > 0; j--) {
         document.write('&nbsp');
      }
      for (let j = (height - i) * 2 - 1; j > 0; j--) {
         document.write('*');
      }
      for (let j = i; j < height - 1; j++) {
         document.write('&nbsp');
      }
      document.write("<br>");
   }

}

document.write("<hr>");


//Порожній прямокутник
document.write("Порожній квадрат <br> <br>");
for (let i = 0; i < height; i++) {
   if (i % (height - 1) === 0) {
      for (let j = 0; j < height; j++) {
         document.write('*');
      }
   } else {
      document.write('*');
      for (let j = 2; j < height; j++) {
         document.write('&nbsp');
      }
      document.write('*');
   }
   document.write("<br>");
}
document.write("<hr>");