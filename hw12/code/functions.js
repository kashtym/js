import { pizzaUser, pizzaBD } from "./data-pizza.js";

const table = document.querySelector(".table");

const pizzaSelectSize = (e) => {
    if (e.target.tagName === "INPUT" && e.target.checked) {
        pizzaUser.size = pizzaBD.size.find((el) => el.name === e.target.id);
    }
    show(pizzaUser);
};

function show(pizza) {
    const price = document.getElementById("price");
    const sauce = document.getElementById("sauce");
    const topping = document.getElementById("topping");

    //count Total price
    let totalPrice = 0;
    if (pizza.size !== "") {
        totalPrice += parseFloat(pizza.size.price);
    }
    if (pizza.sauce !== "") {
        totalPrice += parseFloat(pizza.sauce.price);
    }
    if (pizza.topping.length) {
        totalPrice += pizza.topping.reduce((a, b) => a + b.price * b.count, 0);
    }
    price.innerText = totalPrice;

    //show sauce
    if (pizza.sauce !== "") {
        sauce.innerHTML = `<span class="topping">${pizza.sauce.productName}</span>`;
    }

    //show topping
    if (Array.isArray(pizza.topping)) {
        //clear topings in HTML
        topping.innerHTML = "";

        pizza.topping.forEach((el) => {
            //create span and button
            const span = document.createElement("span");
            const button = document.createElement("button");

            span.textContent = `${el.productName}: ${el.count}шт.`;
            span.classList.add("topping");
            span.dataset.toppingName = el.name;

            button.textContent = "X";
            button.addEventListener("click", removeTopping);

            span.appendChild(button);
            topping.appendChild(span);
        });
    }

    pizzaUser.price = totalPrice;

    pizzaUser.data = new Date();
}

function removeTopping() {
    let toppingName = this.parentNode.dataset.toppingName;
    //change count removed image
    pizzaUser.topping.forEach((elem) => {
        if (elem.name === toppingName) {
            elem.count--;
            //remove topping image
            if (elem.count < 1) {
                [...table.children].forEach((elem) => {
                    if (elem.dataset.toppingname === toppingName) {
                        elem.remove();
                    }
                });
            }
        }
    });

    //remove topping if count <=0
    pizzaUser.topping = pizzaUser.topping.filter((elem) => elem.count > 0);

    show(pizzaUser);
}

function selectInput(input, data, prop) {
    input.className = "";
    input.classList.add("success");
    data[prop] = input.value;
}

function dragDrop(e) {
    if (e.preventDefault) e.preventDefault();
    if (e.stopPropagation) e.stopPropagation();
    this.classList.remove("dragbordergreen");
    let [toppingID, toppingSRC] = JSON.parse(e.dataTransfer.getData("topping"));
    switch (toppingID) {
        case "sauceClassic":
            addSauceUserPizza(toppingID);
            addSauceIMG(toppingSRC);
            break;
        case "sauceBBQ":
            addSauceUserPizza(toppingID);
            addSauceIMG(toppingSRC);
            break;
        case "sauceRikotta":
            addSauceUserPizza(toppingID);
            addSauceIMG(toppingSRC);
            break;
        case "moc1":
            addToppingUserPizza(toppingID);
            addToppingIMG(toppingSRC, toppingID);
            break;
        case "moc2":
            addToppingUserPizza(toppingID);
            addToppingIMG(toppingSRC, toppingID);
            break;
        case "moc3":
            addToppingUserPizza(toppingID);
            addToppingIMG(toppingSRC, toppingID);
            break;
        case "telya":
            addToppingUserPizza(toppingID);
            addToppingIMG(toppingSRC, toppingID);
            break;
        case "vetch1":
            addToppingUserPizza(toppingID);
            addToppingIMG(toppingSRC, toppingID);
            break;
        case "vetch2":
            addToppingUserPizza(toppingID);
            addToppingIMG(toppingSRC, toppingID);
            break;
    }
    show(pizzaUser);
}

function addSauceUserPizza(name) {
    pizzaUser.sauce = pizzaBD.sauce.find((el) => el.name === name);
}

function addToppingUserPizza(name) {
    let topping = pizzaBD.topping.find((el) => el.name === name);
    if (pizzaUser.topping.length) {
        if ([...pizzaUser.topping].every((el) => el.name !== name)) {
            topping.count = 1;
            pizzaUser.topping.push(topping);
        } else {
            [...pizzaUser.topping].forEach((el) => {
                if (el.name === name) {
                    el.count++;
                }
            });
        }
    } else {
        topping.count = 1;
        pizzaUser.topping.push(topping);
    }
}

function addSauceIMG(src) {
    [...table.children].forEach((elem) => {
        if (elem.dataset.sauce) {
            elem.remove();
        }
    });
    table.insertAdjacentHTML(
        "beforeend",
        `<img src="${src}" class="sauce" data-sauce="true">`
    );
}

function addToppingIMG(src, name) {
    if (
        [...table.children].every((elem) => {
            return elem.attributes.src.textContent !== src;
        })
    ) {
        table.insertAdjacentHTML(
            "beforeend",
            `<img src="${src}" data-toppingname="${name}">`
        );
    }
}

const validate = (pattern, value) => pattern.test(value);

function getRandomIntInclusive(min, max) {
    return Math.floor(Math.random() * (max - min + 1) + min);
}

export {
    pizzaSelectSize,
    show,
    validate,
    selectInput,
    dragDrop,
    getRandomIntInclusive,
    table,
};
