/*
Реалізуйте клас Worker (Працівник), який матиме такі властивості: name (ім'я), surname (прізвище),
rate (ставка за день роботи), days (кількість відпрацьованих днів).
Також клас повинен мати метод getSalary(), який виводитиме зарплату працівника.
Зарплата - це добуток (множення) ставки rate на кількість відпрацьованих днів days.
*/
class Worker {
    constructor(name, surname, rate, days) {
        this.name = name;
        this.surname = surname;
        this.rate = rate;
        this.days = days;
    }

    getSalary() {
        return this.rate * this.days;
    }
}

let ivan = new Worker("Ivan", "Maslo", 100, 20);
console.log(ivan.getSalary());

/*
Реалізуйте клас MyString, який матиме такі методи: метод reverse(),
який параметром приймає рядок, а повертає її в перевернутому вигляді, метод ucFirst(),
який параметром приймає рядок, а повертає цей же рядок, зробивши його першу літеру великою
та метод ucWords, який приймає рядок та робить заголовною першу літеру кожного слова цього рядка.
*/

class MyString {
    reverse(str) {
        let result = "";
        for (let i = str.length - 1; i >= 0; i--) {
            result += str[i];
        }
        return result;
    }

    ucFirst(str) {
        return str.length > 0 ? str[0].toUpperCase() + str.slice(1) : "";
    }

    ucWords(str) {
        let strArr = str.split(" ");
        for (let i = 0; i < strArr.length; i++) {
            strArr[i] = this.ucFirst(strArr[i]);
        }
        return strArr.join(" ");
    }
}

let str = new MyString();
console.log(
    str.reverse("lorem ipsum dolor sit amet consectetur adipisicing elit.")
);
console.log(
    str.ucFirst("lorem ipsum dolor sit amet consectetur adipisicing elit.")
);
console.log(
    str.ucWords("lorem ipsum dolor sit amet consectetur adipisicing elit.")
);

/*
Створіть клас Phone, який містить змінні number, model і weight.
Створіть три екземпляри цього класу.
Виведіть на консоль значення їх змінних.
Додати в клас Phone методи: receiveCall, має один параметр - ім'я. Виводить на консоль повідомлення "Телефонує {name}". Метод getNumber повертає номер телефону. Викликати ці методи кожного з об'єктів.
*/
class Phone {
    constructor(number, model, weight) {
        this.number = number;
        this.model = model;
        this.weight = weight;
    }

    receiveCall(name) {
        console.log(`Телефонує ${name}`);
    }

    getNumber() {
        return this.number;
    }
}

let phones = [
    new Phone("+380631111111", "iPhone XS", "177g"),
    new Phone("+380662222222", "Samsung Galaxy S21", "177g"),
    new Phone("+380673333333", "Motorola G32", "184g"),
];

for (let phone of phones) {
    console.log(phone.number, phone.model, phone.weight);
    phone.receiveCall("Ivan");
    console.log(phone.getNumber());
}

/*
Створити клас Car , Engine та Driver.
Клас Driver містить поля - ПІБ, стаж водіння.
Клас Engine містить поля – потужність, виробник.
Клас Car містить поля – марка автомобіля, клас автомобіля, вага, водій типу Driver, мотор типу Engine. Методи start(), stop(), turnRight(), turnLeft(), які виводять на друк: "Поїхали", "Зупиняємося", "Поворот праворуч" або "Поворот ліворуч". А також метод toString(), який виводить повну інформацію про автомобіль, її водія і двигуна.

Створити похідний від Car клас - Lorry (вантажівка), що характеризується також вантажопідйомністю кузова.
Створити похідний від Car клас - SportCar, який також характеризується граничною швидкістю.
https://storage.googleapis.com/www.examclouds.com/oop/car-ierarchy.png
*/
class Car {
    carBrand = "Toyota";
    carClass = "С";
    carWeight = "1200 kg";
    carEngine = new Engine();
    carDriver = new Driver();

    start() {
        document.write("Поїхали <br>");
    }

    stop() {
        document.write("Зупиняємося <br>");
    }

    turnRight() {
        document.write("Поворот праворуч <br>");
    }

    turnLeft() {
        document.write("Поворот ліворуч <br>");
    }

    // Рекурсивно перебирає об'єкт та повертає одномірний масив з усіма властивостями у вигляді "key : value"
    toArray(obj) {
        let arr = [];
        for (let key in obj) {
            if (typeof obj[key] === "object") {
                arr.push(...this.toArray(obj[key]));
            } else {
                arr.push(`${key} : ${obj[key]}`);
            }
        }
        return arr;
    }

    // Виводить построково всі властивості об'єкта з масиву властивостей (включаючи вкладені)
    toString() {
        let arr = this.toArray(this);
        for (let val of arr) {
            document.write(`${val} <br>`);
        }
    }
}

class Engine {
    engPower = "100 hp";
    engManufact = "Toyota";
}

class Driver {
    drivFullName = "Shevchenko Taras Hryhorovych";
    drivExper = "24 years";
}

let myCar = new Car();
myCar.start();
myCar.stop();
myCar.turnRight();
myCar.turnLeft();
myCar.toString();

class Lorry extends Car {
    constructor(carrying) {
        super();
        this.carCarrying = carrying;
    }
}

let myLorry = new Lorry("15ton");
//myLorry.toString();

class SportCar extends Car {
    constructor(speed) {
        super();
        this.carSpeed = speed;
    }
}

let mySportCar = new SportCar("300 km/h");
//mySportCar.toString();

/*
Створити клас Animal та розширюючі його класи Dog, Cat, Horse.
Клас Animal містить змінні food, location і методи makeNoise, eat, sleep. Метод makeNoise, наприклад, може виводити на консоль "Така тварина спить".
Dog, Cat, Horse перевизначають методи makeNoise, eat.
Додайте змінні до класів Dog, Cat, Horse, що характеризують лише цих тварин.
Створіть клас Ветеринар, у якому визначте метод void treatAnimal(Animal animal). Нехай цей метод роздруковує food і location тварини, що прийшла на прийом.
У методі main створіть масив типу Animal, в який запишіть тварин всіх типів, що є у вас. У циклі надсилайте їх на прийом до ветеринара.
*/

class Animal {
    constructor(food, location) {
        this.food = food;
        this.location = location;
    }

    makeNoise() {
        return "Тварина видає звук";
    }

    eat() {
        return "Улюблена їжа";
    }

    sleep() {
        return "Тварина спить";
    }
}

class Dog extends Animal {
    constructor(food, location, countTornToys) {
        super(food, location);
        this.countTornToys = countTornToys;
    }

    makeNoise() {
        return `${super.makeNoise()} - "Гав!"`;
    }

    eat() {
        return `Улюблена їжа - ${this.food}`;
    }
}

class Cat extends Animal {
    constructor(food, location, countCatchMouse) {
        super(food, location);
        this.countCatchMouse = countCatchMouse;
    }

    makeNoise() {
        return `${super.makeNoise()} - "Няу!"`;
    }

    eat() {
        return `Улюблена їжа - ${this.food}`;
    }
}

class Horse extends Animal {
    constructor(food, location, countHorseshoe) {
        super(food, location);
        this.countHorseshoe = countHorseshoe;
    }

    makeNoise() {
        return `${super.makeNoise()} - "Іго-го!"`;
    }

    eat() {
        return `Улюблена їжа - ${this.food}`;
    }
}

class Veterinarian {
    treatAnimal(animal) {
        console.log(
            `їжа - ${animal.food}, місце проживання - ${
                animal.location
            }, ${animal.makeNoise()}`
        );
    }
    main() {
        let animalArray = [
            new Dog("Кістка", "Будка", 10),
            new Cat("Риба", "Корзина", 0),
            new Horse("Овес", "Конюшня", 4),
        ];
        for (let animal of animalArray) {
            this.treatAnimal(animal);
        }
    }
}

let doctor = new Veterinarian();
doctor.main();
