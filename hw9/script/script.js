/*
Завдання №1:
Створіть програму секундомір.
* Секундомір матиме 3 кнопки "Старт, Стоп, Скидання"
* При натисканні на кнопку стоп фон секундоміра має бути червоним, старт - зелений, скидання - сірий * Виведення лічильників у форматі ЧЧ:ММ:СС
* Реалізуйте Завдання використовуючи синтаксис ES6 та стрілочні функції
*/

let counter = 0,
    flag = false,
    intervalHandler;

//Створюємо посилання для звертання до елементів дисплея та кнопок
let display = document.querySelector(".stopwatch-display");
let [...displayArr] = display.children;
let buttons = document.querySelector(".stopwatch-control");
let [...buttonsArr] = buttons.children;

//Функція додає клас "color" до елемента "element" та видаляє інші з трьох можливих
let addColorStyle = (element, color) => {
    let colorArr = ["red", "green", "silver"];
    for (let col of colorArr) {
        if (col !== color) {
            element.classList.remove(col);
        }
    }
    element.classList.add(color);
};

//Функція збільшує лічільник та записує його дані на дисплей у форматі ГГ:ХХ:СС
const countAndSetTime = () => {
    counter++;
    displayArr[2].textContent = (counter % 60).toString().padStart(2, 0);
    displayArr[1].textContent = (Math.floor(counter / 60) % 60)
        .toString()
        .padStart(2, 0);
    displayArr[0].textContent = (Math.floor(counter / 60 / 60) % 24)
        .toString()
        .padStart(2, 0);
};

//Натискання на кнопку "Start" міняє колір фону, запускає інтервал з контролем, що він вже не запущений
buttonsArr[0].onclick = () => {
    addColorStyle(display, "green");
    if (!flag) {
        intervalHandler = setInterval(countAndSetTime, 1000);
        flag = true;
    }
};

//Натискання на кнопку "Stop" міняє колір фону, зупиняє інтервал
buttonsArr[1].onclick = () => {
    addColorStyle(display, "red");
    clearInterval(intervalHandler);
    flag = false;
};
//Натискання на кнопку "Reset" міняє колір фону, зупиняє інтервал, скидаємо лічільник
buttonsArr[2].onclick = () => {
    addColorStyle(display, "silver");
    clearInterval(intervalHandler);
    flag = false;
    counter = -1;
    countAndSetTime();
};

/*
Завдання №2:
Реалізуйте програму перевірки телефону
*Використовуючи JS Створіть поле для введення телефону та кнопку збереження
*Користувач повинен ввести номер телефону у форматі 000-000-00-00
*Після того як користувач натискає кнопку зберегти перевірте правильність введення номера, якщо номер правильний зробіть зелене тло і використовуючи document.location переведіть користувача на сторінку https://risovach.ru/upload/2013/03/mem/toni-stark_13447470_big_.jpeg якщо буде помилка, відобразіть її в діві до input
*/

let phoneNumDiv = document.querySelector(".phoneNumber");

//функція повертає посилання на дочірній елемент
let linkChildrenElement = (element, chElement) => {
    [...chElementArr] = element.children;
    for (let i = 0; i < chElementArr.length; i++) {
        if (chElementArr[i].nodeName === chElement.toUpperCase()) {
            return chElementArr[i];
        }
    }
};

//функція видаляє дочірній елемент
let removeChildrenElement = (element, chElement) => {
    if (linkChildrenElement(element, chElement)) {
        let removeElem = linkChildrenElement(element, chElement);
        removeElem.remove();
    }
};

// функція перевіряє валідність номера та переводить на іншу сторінку у разі успіху або виводить повідомлення помилки
let validatePhoneNum = () => {
    let inputPhoneNum = linkChildrenElement(phoneNumDiv, "input");
    let phoneNum = inputPhoneNum.value;
    if (/^\d\d\d-\d\d\d-\d\d-\d\d$/.test(phoneNum)) {
        inputPhoneNum.classList.add("green");
        removeChildrenElement(phoneNumDiv, "div");
        setTimeout(() => {
            document.location =
                "https://risovach.ru/upload/2013/03/mem/toni-stark_13447470_big_.jpeg";
        }, 1000);
    } else {
        inputPhoneNum.classList.add("red");
        removeChildrenElement(phoneNumDiv, "div");
        let div = document.createElement("div");
        div.textContent =
            "Увага! Невірно введений номер! Введіть номер у форматі: 099-999-99-99";
        inputPhoneNum.before(div);
    }
};
