//Створити об'єкт "Документ", де визначити властивості "Заголовок, тіло, футер, дата"
const documentPage = {
   header: "",
   body: "",
   footer: "",
   date: ""
};

//Створити вкладений об'єкт - "Додаток"
documentPage["addition"] = new Object;

//Створити об'єкт "Додаток",  зі вкладеними об'єктами, "Заголовок, тіло, футер, дата"
documentPage.addition.header = "";
documentPage.addition.body = "";
documentPage.addition.footer = "";
documentPage.addition.date = "";

//Створити методи для заповнення та відображення документа.використовуючі оператор in
//Заповнюю властивості типами даних
documentPage.fillType = function() {
   for (let prop in documentPage) {
      if (typeof (documentPage[prop]) !== "object" && typeof (documentPage[prop]) !== "function") {
         documentPage[prop] = typeof (documentPage[prop]);
      } else if(typeof (documentPage[prop]) === "object") {
         for (let propNest in documentPage[prop]) {
            documentPage[prop][propNest] = typeof (documentPage[prop][propNest]);
         }
      }
   }
}

//Відображаю в дакументи значення властивостей
documentPage.writeAll = function() {
   for (let prop in documentPage) {
      if (typeof (documentPage[prop]) !== "object" && typeof (documentPage[prop]) !== "function") {
         document.write(`${documentPage[prop]} <br>`);
      } else if (typeof (documentPage[prop]) === "object") {
         document.write(`<hr> addition: <br>`);
         for (let propNest in documentPage[prop]) {
            document.write(`${documentPage[prop][propNest]} <br>`);
         }
      }
   }
}

documentPage.fillType();
documentPage.writeAll();