/*
Створіть масив styles з елементами «Джаз» та «Блюз».
Додайте "Рок-н-рол" в кінець.
Замініть значення всередині на «Класика». Ваш код для пошуку значення всередині повинен працювати для масивів з будь - якою довжиною.
Видаліть перший елемент масиву та покажіть його.
Вставте «Реп» та «Реггі» на початок масиву.
*/

//Створіть масив styles з елементами «Джаз» та «Блюз»
const styles = ["Джаз", "Блюз"];
document.write(`Створіть масив styles з елементами «Джаз» та «Блюз»: <br>`);
document.write(`<span style="color: red">styles = [${styles}]</span> <hr>`);

//Додайте "Рок-н-рол" в кінець
styles.push("Рок-н-рол");
document.write(`Додайте "Рок-н-рол" в кінець: <br>`);
document.write(`<span style="color: red">styles = [${styles}]</span> <hr>`);

// Замініть значення всередині на «Класика». Ваш код для пошуку значення всередині повинен працювати для масивів з будь - якою довжиною
styles.splice(Math.floor(styles.length / 2), 1, "Класика");
document.write(`Замініть значення всередині на «Класика». Ваш код для пошуку значення всередині повинен працювати для масивів з будь - якою довжиною: <br>`);
document.write(`<span style="color: red">styles = [${styles}]</span> <hr>`);

//Видаліть перший елемент масиву та покажіть його
document.write(`Видаліть перший елемент масиву та покажіть його: <br>`);
document.write(`<span style="color: red">styles[0] = ${styles.shift()}</span> <br>`);
document.write(`<span style="color: red">styles = [${styles}]</span> <hr>`);

//Вставте «Реп» та «Реггі» на початок масиву
styles.unshift("Реп", "Реггі");
document.write(`Вставте «Реп» та «Реггі» на початок масиву: <br>`);
document.write(`<span style="color: red">styles = [${styles}]</span> <hr>`);

