//Завдвння №1
// Дано інпути. Зробіть так, щоб усі інпути втрати фокусу перевіряли свій вміст на правильну кількість символів. Скільки символів має бути в інпуті, зазначається в атрибуті data-length. Якщо вбито правильну кількість, то межа інпуту стає зеленою, якщо неправильна – червоною.

// Посилання на всі input
let inputs = document.querySelectorAll(".task1");

//функція валідації введених даних (кількість введених символів дорівнює 'data-length')
function valideteInput() {
    if (this.value.length === +this.dataset.length) {
        this.classList.add("greenborder");
        this.classList.remove("redborder");
    } else {
        this.classList.add("redborder");
        this.classList.remove("greenborder");
    }
}

//додаємо 'слухач' події на втрату фокусу з активацією функції валідації

for (let input of inputs) {
    input.addEventListener("blur", valideteInput);
}

/*Завдвння №2
При завантаженні сторінки показати користувачеві поле введення (`input`) з написом `Price`. Це поле буде служити для введення числових значень
Поведінка поля має бути такою:
- При фокусі на полі введення – у нього має з'явитися рамка зеленого кольору. При втраті фокусу вона пропадає.
- Коли забрали фокус з поля - його значення зчитується, над полем створюється `span`, в якому має бути виведений текст:
.
Поруч із ним має бути кнопка з хрестиком (`X`). Значення всередині поля введення фарбується зеленим.
- При натисканні на `Х` - `span` з текстом та кнопка `X` повинні бути видалені.
- Якщо користувач ввів число менше 0 - при втраті фокусу підсвічувати поле введення червоною рамкою,
під полем виводити фразу - `Please enter correct price`. `span` зі значенням при цьому не створюється.
*/

// Посилання на input
let input = document.querySelector("input.task2");

//Додаємо надпис `Price` у 'placeholder'
input.placeholder = "Price";

// додаємо зелену рамку при фокусі
input.addEventListener("focus", () => input.classList.add("greenborder"));

// видаляємо зелену рамку при втраті фокусу
input.addEventListener("blur", () => input.classList.remove("greenborder"));

// виконуємо валідацію при втраті фокусу
input.addEventListener("blur", validatePrice);

//створює `span`(якщо ойго не існує) значення присвоюється з `input`. Створюється кнопка, яка по кліку запускає функцію видалення
function createOrChangePriceValue() {
    let span, button;
    if (!document.querySelector("span.task2")) {
        span = document.createElement("span");
        button = document.createElement("button");
        span.classList.add("task2");
        button.textContent = "X";
        button.classList.add("button", "task2");
        button.addEventListener("click", removePriceValue);
        input.before(span);
        span.after(button);
    } else {
        span = document.querySelector("span.task2");
    }
    span.textContent = `Total price: ${parseFloat(input.value)}$`;
}

//видаляє кнопку та `span`
function removePriceValue() {
    if (document.querySelector("span.task2")) {
        document.querySelector("span.task2").remove();
        document.querySelector("button.task2").remove();
    }
}
//створює `p`(якщо ойго не існує) з попередженням
function createErrorInscription() {
    if (!document.querySelector("p.task2")) {
        let p = document.createElement("p");
        p.classList.add("task2");
        p.textContent = "Please enter correct price!";
        input.after(p);
    }
}
//видаляє `p` з попередженням
function removeErrorInscription() {
    if (document.querySelector("p.task2")) {
        document.querySelector("p.task2").remove();
    }
}
// перевіряє що введено число та його значення > 0
function validatePrice() {
    if (
        parseFloat(input.value) > 0 &&
        parseFloat(input.value).toString().length === input.value.length
    ) {
        createOrChangePriceValue();
        removeErrorInscription();
        input.classList.add("greentext");
        input.classList.remove("redtext");
    } else {
        removePriceValue();
        createErrorInscription();
        input.classList.add("redtext");
        input.classList.remove("greentext");
    }
}
