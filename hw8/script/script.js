//Завдання №1
// Створіть 2 інпути та одну кнопку. Зробіть так, щоб інпути обмінювалися вмістом.

const changeInput = () => {
    const input1 = document.getElementById("input1");
    const input2 = document.getElementById("input2");

    let x = input1.value;
    input1.value = input2.value;
    input2.value = x;
};

//Завдання №2
//Створіть 5 див на сторінці потім використовуючи getElementsByTagName і forEath поміняйте дивам колір фону.

window.onload = () => {
    const divAll = [...document.getElementsByTagName("div")];
    divAll.forEach((element) => {
        element.style.backgroundColor = "rgb(150, 180, 150)";
    });
};

//Завдання №3
//Створіть багаторядкове поле для введення тексту та кнопки. Після натискання кнопки користувачем програма повинна згенерувати тег div з текстом, який був у багаторядковому полі. багаторядкове поле слід очистити після переміщення інформації

const createDiv = () => {
    const inputTextArea = document.getElementById("inputTextArea");
    const buttonTextArea = document.getElementById("buttonTextArea");
    let val = inputTextArea.value;
    inputTextArea.value = "";
    let div = document.createElement("div");
    div.textContent = val;
    if (val !== "") {
        buttonTextArea.after(div);
    }
};

//Завдання №4
// Створіть картинку та кнопку з назвою "Змінити картинку" зробіть так щоб при авантаженні сторінки була картинка https://itproger.com/img/courses/1476977240.jpg При натисканні на кнопку вперше картинка замінилася на https://itproger.com/img/courses/1476977488.jpg при другому натисканні щоб картинка замінилася на https://upload.wikimedia.org/wikipedia/commons/thumb/9/99/Unofficial_JavaScript_logo_2.svg/1200px-Unofficial_JavaScript_logo_2.svg.png

let countChangeImg = 0;
changeImage = () => {
    let imgArr = [
        "https://itproger.com/img/courses/1476977488.jpg",
        "https://upload.wikimedia.org/wikipedia/commons/thumb/9/99/Unofficial_JavaScript_logo_2.svg/1200px-Unofficial_JavaScript_logo_2.svg.png",
        "https://itproger.com/img/courses/1476977240.jpg",
    ];
    const img = document.getElementById("img");
    switch (countChangeImg % 3) {
        case 0:
            img.src = imgArr[0];
            break;
        case 1:
            img.src = imgArr[1];
            break;
        case 2:
            img.src = imgArr[2];
            break;
    }
    countChangeImg++;
};

//Завдання №5
// Створіть на сторінці 10 параграфів і робіть так, щоб при натисканні на параграф він зникав

function removeElem() {
    this.remove();
}

let parArr = [...document.getElementsByClassName("paragraph")];

for (let i = 0; i < parArr.length; i++) {
    parArr[i].addEventListener("click", removeElem);
}
