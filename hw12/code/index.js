import {
    pizzaSelectSize,
    show,
    validate,
    table,
    selectInput,
    dragDrop,
    getRandomIntInclusive,
} from "./functions.js";
import { pizzaUser, pizzaBD } from "./data-pizza.js";

document.querySelectorAll(".grid input").forEach((input) => {
    if (
        input.type === "text" ||
        input.type === "tel" ||
        input.type === "email"
    ) {
        input.addEventListener("change", () => {
            if (
                input.type === "text" &&
                validate(/^[А-я-іїґє]{2,}$/i, input.value)
            ) {
                selectInput(input, pizzaUser, "userName");
            } else if (
                input.type === "tel" &&
                validate(/^\+380\d{9}$/, input.value)
            ) {
                selectInput(input, pizzaUser, "userPhone");
            } else if (
                input.type === "email" &&
                validate(
                    /^[a-z0-9_.]{3,}@[a-z0-9._]{2,}\.[a-z.]{2,9}$/i,
                    input.value
                )
            ) {
                selectInput(input, pizzaUser, "userEmail");
            } else {
                input.classList.add("error");
            }
        });
    } else if (input.type === "reset") {
        input.addEventListener("click", () => {});
    } else if (input.type === "button") {
        input.addEventListener("click", () => {
            localStorage.userInfo = JSON.stringify(pizzaUser);
            window.location.href = "./thank-you/index.html";
        });
    }
});

document.querySelector("#pizza").addEventListener("click", pizzaSelectSize);
document.querySelector("#banner").addEventListener("mouseover", function () {
    let xMax = window.innerWidth - this.offsetWidth;
    let yMax = window.innerHeight - this.offsetHeight;

    this.style.right = `${getRandomIntInclusive(0, xMax)}px`;
    this.style.bottom = `${getRandomIntInclusive(0, yMax)}px`;
});

const ingridients = document.querySelectorAll(".draggable");

ingridients.forEach((elem) => {
    elem.addEventListener("dragstart", function (e) {
        this.classList.add("dragscale");
        e.dataTransfer.effectAllowed = "move";
        e.dataTransfer.setData(
            "topping",
            JSON.stringify([this.id, this.attributes.src.textContent])
        );
    });
    elem.addEventListener("dragend", function () {
        this.classList.remove("dragscale");
    });
});

table.addEventListener("dragenter", function () {
    this.classList.add("dragbordergreen");
});

table.addEventListener("dragleave", function () {
    this.classList.remove("dragbordergreen");
});

table.addEventListener("dragover", (e) => {
    if (e.preventDefault) {
        e.preventDefault();
        return false;
    }
});

table.addEventListener("drop", dragDrop);

show(pizzaUser);
