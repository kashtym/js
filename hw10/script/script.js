/*
* У папці calculator дана верстка макета калькулятора. 
Потрібно зробити цей калькулятор робочим.
* При натисканні на клавіші з цифрами - набір введених цифр має бути показаний на табло калькулятора.
* При натисканні на знаки операторів (`*`, `/`, `+`, `-`) на табло нічого не відбувається - програма чекає введення другого числа для виконання операції.
* Якщо користувач ввів одне число, вибрав оператор і ввів друге число, то при натисканні як кнопки `=`, так і будь-якого з операторів, в табло повинен з'явитися результат виконання попереднього виразу.
* При натисканні клавіш `M+` або `M-` у лівій частині табло необхідно показати маленьку букву `m` - це означає, що в пам'яті зберігається число. Натискання на MRC покаже число з пам'яті на екрані. Повторне натискання `MRC` має очищати пам'ять.
*/

/*
1. Зєжнати верстку з джс 
Потрібно зробити цей калькулятор робочим.
2. Знайти всі кнопки та спрбувати їх вивести у консоль
3. Записти перші числа в память коду 
4. вивести числа на екран
5. додати знаки ар. операцій 
6. Знайти другі числа 
7. Вивести другі числа 
8. Вивести результат операції
*/

const calculate = {
    operand1: "",
    sign: "",
    operand2: "",
    rez: "",
    mem: "",
};

// https://regexr.com/

// виводить значення на дісплей
function show(v) {
    const d = document.querySelector(".display input");

    d.value = v;
}

// валідація по введенному виразу
const validate = (r, v) => r.test(v);

// очищає значення змінних та дісплей
const clear = () => {
    calculate.operand1 = "";
    calculate.operand2 = "";
    calculate.sign = "";
    calculate.rez = "";
    show(calculate.operand1);
};

// виконує математичну операцію, записує в змінну результат та виводить його на екран
const equals = () => {
    let num1 = parseFloat(calculate.operand1);
    let num2 = parseFloat(calculate.operand2);

    switch (calculate.sign) {
        case "+":
            calculate.rez = (num1 + num2).toString();
            break;
        case "-":
            calculate.rez = (num1 - num2).toString();
            break;
        case "*":
            calculate.rez = (num1 * num2).toString();
            break;
        case "/":
            calculate.rez = (num1 / num2).toString();
            break;
    }
    calculate.operand1 = "";
    calculate.operand2 = "";
    show(calculate.rez);
};

//вводимо числове значення
const addNum = (num) => {
    //якщо нема знака операції - вводимо перше число
    if (calculate.sign === "") {
        if (calculate.operand1.length < 17) {
            calculate.operand1 += num;
        }
        if (num === "0" && calculate.operand1.length === 1) {
            calculate.operand1 += ".";
        }
        calculate.rez = "";
        show(calculate.operand1);
        //якщо є знак операції - вводимо друге число
    } else {
        if (calculate.operand2.length < 17) {
            calculate.operand2 += num;
        }
        if (num === "0" && calculate.operand2.length === 1) {
            calculate.operand2 += ".";
        }
        show(calculate.operand2);
    }
};

// вводимо знак операції
const addSign = (sign) => {
    //якщо введено 2 числа - проводимо обчислення
    if (calculate.operand2 !== "") {
        equals();
    }
    //якщо є результат обчислення та активується знак операції, то результат записуємо як перше число для подальших операцій
    if (calculate.rez !== "") {
        calculate.operand1 = calculate.rez;
        calculate.rez = "";
    }
    //якщо нема першого числа, то в перше число записуємо 0
    if (calculate.operand1 === "") {
        calculate.operand1 = "0";
    }
    calculate.sign = sign;
};

//вводимо крапку
const addDot = () => {
    //якщо нема знака операції - додаємо до першого числа
    if (calculate.sign === "") {
        if (calculate.operand1 === "") {
            calculate.operand1 += "0.";
        }
        if (
            calculate.operand1.length < 17 &&
            calculate.operand1.indexOf(".") === -1
        ) {
            calculate.operand1 += ".";
        }
        show(calculate.operand1);
        //якщо є знак операції - вводимо друге число
    } else {
        if (calculate.operand2 === "") {
            calculate.operand2 += "0.";
        }
        if (
            calculate.operand2.length < 17 &&
            calculate.operand2.indexOf(".") === -1
        ) {
            calculate.operand2 += ".";
        }
        show(calculate.operand2);
    }
};

//кнопки пам'яті
const addMemory = (m) => {
    let num1 = calculate.mem ? parseFloat(calculate.mem) : 0;
    let num2 = parseFloat(document.querySelector(".display input").value);
    if (m === "m+" && num2) {
        calculate.mem = (num1 + num2).toString();
    } else if (m === "m-" && num2) {
        calculate.mem = (num1 - num2).toString();
    }
};

// показати що у пам'яті
const showMemory = () => {
    if (memFlag) {
        calculate.mem = "";
    } else {
        if (calculate.mem) {
            if (calculate.sign === "") {
                calculate.rez = calculate.mem;
                show(calculate.rez);
            } else {
                calculate.operand2 = calculate.mem;
                show(calculate.operand2);
            }
        }
    }
};

//флаг визову вмісту пам'яті для видалення по другому натисканню
let memFlag;

//обробка натискання кнопок
document.querySelector(".keys").addEventListener("click", (e) => {
    if (validate(/\d/, e.target.value)) {
        addNum(e.target.value);
        console.log(calculate);
        memFlag = false;
    } else if (validate(/^[+\-/*]$/, e.target.value)) {
        addSign(e.target.value);
        console.log(calculate);
        memFlag = false;
    } else if (validate(/=/, e.target.value)) {
        equals();
        calculate.sign = "";
        console.log(calculate);
        memFlag = false;
    } else if (validate(/C/, e.target.value)) {
        clear();
        console.log(calculate);
        memFlag = false;
    } else if (validate(/\./, e.target.value)) {
        addDot();
        console.log(calculate);
        memFlag = false;
    } else if (validate(/^m[+-]/, e.target.value)) {
        addMemory(e.target.value);
        console.log(calculate);
        memFlag = false;
    } else {
        showMemory();
        memFlag = true;
        console.log(calculate);
    }

    // Активуємо кнопку дорівнює при наявності другого числа
    if (calculate.operand2 !== "") {
        document.querySelector(
            ".keys"
        ).lastElementChild.lastElementChild.disabled = false;
    } else {
        document.querySelector(
            ".keys"
        ).lastElementChild.lastElementChild.disabled = true;
    }

    // Активуємо іконку пам'яті, якщо в неї щось записано
    if (calculate.mem !== "") {
        document.querySelector(".display span").textContent = "M";
    } else {
        document.querySelector(".display span").textContent = "";
    }
});
