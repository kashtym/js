startRound = () => {
    document.getElementById("buttonStart").remove();
    let label = document.createElement("span");
    label.textContent = "Введіть діаметр кола: ";
    let input = document.createElement("input");
    input.placeholder = "50...150";
    let button = document.createElement("button");
    button.textContent = "Намалювати";
    document.body.append(label, input, button);
    button.addEventListener("click", createRounds);
    console.dir(input);
};

function createRounds() {
    let [label] = document.getElementsByTagName("span");
    let [input] = document.getElementsByTagName("input");
    let [button] = document.getElementsByTagName("button");
    let diameter = parseFloat(input.value);
    label.remove();
    input.remove();
    button.remove();
    console.log(diameter);
    document.body.style.display = "grid";
    for (let i = 0; i < 100; i++) {
        let div = document.createElement("div");
        div.style.width = diameter / 2 + "px";
        div.style.height = diameter / 2 + "px";
        div.style.backgroundColor = `hsl(${(Math.random() * 360).toFixed(
            0
        )} 50% 50%)`;
        div.addEventListener("click", removeElem);
        document.body.append(div);
    }
}

function removeElem() {
    this.remove();
}
