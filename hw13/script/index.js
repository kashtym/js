//Link with comments
const url = "https://jsonplaceholder.typicode.com/comments";

// array with comments
let commentsData;

// amount comments per page
const amountComments = 10;
// page comments counter default
let pageCommentsCounter = 1;
// amount pages per pagination page
const amountPagesPagination = 10;

//Comments area
const commentsArea = document.querySelector(".comments");

//request
function request() {
    const req = new XMLHttpRequest();
    req.open("GET", url);
    req.send();
    req.addEventListener("readystatechange", () => {
        if (req.readyState === 4 && req.status >= 200 && req.status < 300) {
            commentsData = JSON.parse(req.responseText);
            commentsArea.innerHTML = "";
            showComments();
            showPagination();
        } else if (req.readyState === 4) {
            commentsArea.innerHTML = `
            <div class="text-center mb-3 text-danger">
                        <strong c>Error!</strong>
                    </div>`;
            throw new Error("Помилка у запиті!");
        }
    });
}

// to show loader
setTimeout(request, 3000);

// show pagination list
function showPagination() {
    //Max amount pages with comments
    const pageCommentsCounterMax = Math.floor(
        commentsData.length / amountComments
    );

    //pagination page counter
    let paginationPageCounter = Math.ceil(
        pageCommentsCounter / amountPagesPagination
    );

    //create nav pagination
    let list = document.createElement("nav");
    list.innerHTML = `
    <ul class="pagination justify-content-end"></ul>
    `;

    //create reduce li
    let li;
    li = createpaginationLi("&laquo;");
    //if show first pagination page reduce li - not active
    if (paginationPageCounter === 1) {
        li.classList.add("disabled");
    } else {
        li.addEventListener("click", paginationButton);
    }
    list.children[0].appendChild(li);

    // create and show pagination li with page numbers
    for (
        let i = 1 + amountPagesPagination * (paginationPageCounter - 1);
        i /* less max amount pages in pagination page or less Max amount pages with comments*/ <=
        Math.min(
            paginationPageCounter * amountPagesPagination,
            pageCommentsCounterMax
        );
        i++
    ) {
        li = createpaginationLi(i);
        if (i === pageCommentsCounter) {
            li.classList.add("active");
        }
        li.addEventListener("click", paginationButton);
        list.children[0].appendChild(li);
    }

    //create increase li
    li = createpaginationLi("&raquo;");
    // if show last pagination page increase li - not active
    if (
        paginationPageCounter ===
        Math.ceil(pageCommentsCounterMax / amountPagesPagination)
    ) {
        li.classList.add("disabled");
    } else {
        li.addEventListener("click", paginationButton);
    }
    list.children[0].appendChild(li);

    //add pagination to comments area
    commentsArea.insertBefore(list, commentsArea.firstChild);
}

function createpaginationLi(text) {
    let elem = document.createElement("li");
    elem.innerHTML = `<a class="page-link" href="#">${text}</a>`;
    elem.classList.add("page-item");
    return elem;
}

function showComments() {
    for (
        i = 0 + (pageCommentsCounter - 1) * amountComments;
        i < amountComments + (pageCommentsCounter - 1) * amountComments;
        i++
    ) {
        let card = document.createElement("div");
        card.classList.add("card", "mb-3");
        card.innerHTML = `<div class="row m-0">
    <div
        class="col-md-4 text-center pt-4 p-0"
        style="background-color: rgba(0, 0, 0, 0.05)"
    >
        <img
            src="./img/user_logo.svg"
            class="h-50"
            alt="..."
        />
        <p class="mt-2">Email: 
            <a href="mailto:" class="link-secondary"
                >${commentsData[i].email}</a
            >
        </p>
    </div>
    <div class="col-md-8 border-3">
        <div class="card-body p-1">
            <div
                class="row text-muted border-bottom mb-3"
            >
                <div class="col px-2">
                    Date: 11.02.2023
                </div>
                <div class="col text-end">#${commentsData[i].id}</div>
            </div>
            <h6>
            ${commentsData[i].name}
            </h6>
            <p class="card-text">
            ${commentsData[i].body}
            </p>
            <div class="border-top pt-1 text-end">
                <button class="btn btn-dark btn-sm">
                    reply
                </button>
            </div>
        </div>
    </div>
</div>`;
        commentsArea.appendChild(card);
    }
}

function paginationButton() {
    //show selected page
    if (parseFloat(this.children[0].textContent)) {
        pageCommentsCounter = parseFloat(this.children[0].textContent);
    }
    //show previous 10 pages with action first
    if (this.children[0].textContent === "«") {
        pageCommentsCounter =
            Math.floor((pageCommentsCounter - 1) / amountPagesPagination - 1) *
                amountPagesPagination +
            1;
    }
    //show next 10 pages with action first
    if (this.children[0].textContent === "»") {
        pageCommentsCounter =
            Math.ceil(pageCommentsCounter / amountPagesPagination) *
                amountPagesPagination +
            1;
    }
    //clear previous comments
    commentsArea.innerHTML = "";
    showComments();
    showPagination();
}
